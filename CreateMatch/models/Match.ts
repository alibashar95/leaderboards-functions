type Match = {
    gameId: string,
    matchType: '1v1' | 'FFA' | 'Teams',
    teams: Team[];
}

type Team = {
    name: string,
    placement: number | string,
    players: Player[],
    totalRating?: number,
    ratingChange?: number
}

type Player = {
    id: string,
    stats: any
}