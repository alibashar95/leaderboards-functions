import { AzureFunction, Context, HttpRequest } from "@azure/functions"
import axios from 'axios';

const INITIAL_RATING = 1000;

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function processed a request.');

    const request = req.body as Match;

    const url = `${process.env.BACKEND_URL}/api/collections`;

    const headers = {
      // The authorization header with the token or credentials
      Authorization: req.headers.authorization,
    };

    let res: FunctionResponse = {};

    if (validateRequest(request, res)) {
        const match = await createMatch(request, url, headers);
        await createTeamsAndManagePlayers(request, match, url, headers);

        res.body = request.teams
        res.status = 201;
    }

    context.res = {
        status: res.status,
        body: res.body
    };
};

export default httpTrigger;

async function createMatch(request: Match, url: string, headers: any) {
    const matchData = {
        "game": request.gameId,
        "matchType": request.matchType
    };

    const response = await axios.post(url + '/matches/records', matchData, { headers })

    return response.data;
}

async function createTeamsAndManagePlayers(request: Match, match: any, url: string, headers: any) {
    for (let i = 0; i < request.teams.length; i++) {
        const t = request.teams[i];

        const teamData = {
            name: t.name,
            placement: t.placement,
            match: match.id,
        };

        const team = (
            await axios.post(url + "/teams/records", teamData, { headers })
        ).data;

        for (let j = 0; j < t.players.length; j++) {
            const p = t.players[j];

            const playerMatchData = {
                player: p.id,
                match: match.id,
                team: team.id,
            };

            await axios.post(url + "/player_matches/records", playerMatchData, {
                headers,
            });

            const playerStats = await axios.get(
                url +
                    `/player_stats/records?filter=player%3D%27${p.id}%27%20%26%26%20game%3D%27${request.gameId}%27&'`
            );

			t.totalRating ??= 0;

            p.stats = playerStats?.data?.items[0];
            t.totalRating += p.stats?.rating ?? INITIAL_RATING;
        };
    }

    calculateElo(request.teams);

    for (let i = 0; i < request.teams.length; i++) {
		const t = request.teams[i];
        for (let j = 0; j < t.players.length; j++) {
			const p = t.players[j];
            let statsData;

			statsData = {
                player: p.id,
                game: request.gameId,
                first_places: (p.stats?.first_places ?? 0) + (t.placement === 1 || t.placement === '1' ? 1 : 0),
                second_places: (p.stats?.second_places ?? 0) + ((request.matchType === 'FFA') && (t.placement === 2 || t.placement === '2') ? 1 : 0),
                third_places: (p.stats?.third_places ?? 0) + ((request.matchType === 'FFA') && (t.placement === 3 || t.placement === '3') ? 1 : 0),
                win_streak: t.placement === 1 || t.placement === '1' ? (p.stats?.win_streak ?? 0) + 1 : 0,
                matches_played: (p.stats?.matches_played ?? 0) + 1,
                rating: (p.stats?.rating ?? INITIAL_RATING) + Math.round((t.ratingChange / (request.teams.length - 1))),
            };

            //Minimum rating: 0
            if (statsData.rating < 0)
                statsData.rating = 0

            if (p.stats) {
                const request = url + `/player_stats/records/${p.stats?.id}`;
                await axios.patch(request, statsData, { headers });
            } else {
                await axios.post(url + `/player_stats/records`, statsData, {
                    headers,
                });
            }

            t.ratingChange = Math.round((t.ratingChange / (request.teams.length - 1)));

			p.stats = statsData
        }
    };
}

function validateRequest(request: Match, res: FunctionResponse): boolean {
    if (hasDuplicates(request.teams)){
        return BadRequest("Cannot choose same player twice", res)
    }

    if (!request.gameId || request.gameId === '') {
        return BadRequest("body.gameId required", res)
    }

    if (!request.matchType || (request.matchType !== '1v1' && request.matchType !== 'Teams' && request.matchType !== 'FFA')) {
        return BadRequest("body.matchType required and must either be '1v1', 'Teams' or 'FFA'", res)
    }

    if (!request.teams || request.teams.length < 2) {
        return BadRequest("body.teams required and at least 2 teams required", res)
    }

    for (let i = 0; i < request.teams.length; i++) {
        if (!request.teams[i].name || request.teams[i].name === '') {
            return BadRequest("body.teams[*].name required", res)
        }

        if (!request.teams[i].placement || request.teams[i].placement === '' || request.teams[i].placement as number <= 0 || request.teams[i].placement === '0') {
            return BadRequest("body.teams[*].placement required and must be larger than 0", res)
        }

        if (!request.teams[i].players || request.teams[i].players.length < 1) {
            return BadRequest("body.teams[*].players required and at least 1 player per team", res)
        }

        for (let j = 0; j < request.teams[i].players.length; j++) {
            if (!request.teams[i].players[j].id || request.teams[i].players[j].id === '') {
                return BadRequest("body.teams[*].players[*].id required", res)
            }
        }
    }

    if ((request.matchType === '1v1' || request.matchType === 'Teams') && !request.teams.some(t => t.placement === 1 || t.placement === '1')){
        return BadRequest("A winner is required", res)
    }

    return true;
}

function BadRequest(error: string, res: FunctionResponse) {
    res.status = 400
    res.body = {error: error}
    return false;
}

function calculateElo(teams: Team[]) {
  const numberOfTeams = teams.length;
  const sortedTeams = teams.sort(((a, b) => (a.placement > b.placement ? 1 : -1)));

  for (let i = 0; i < numberOfTeams; i++) {
    const currentTeam = sortedTeams[i];

    //Match against each team
    for (let j = 0; j < numberOfTeams; j++) {
        if (i === j || sortedTeams[i].placement === sortedTeams[j].placement) continue;

        let currentTeamPlacedHigher = i < j;

        const opponentTeam = sortedTeams[j];

		currentTeam.ratingChange ??= 0

        if (currentTeamPlacedHigher) {
            currentTeam.ratingChange += getEloChange(currentTeam.totalRating, opponentTeam.totalRating);
        } else {
            currentTeam.ratingChange -= getEloChange(opponentTeam.totalRating, currentTeam.totalRating);
        }
    }
  }
}

// https://en.wikipedia.org/wiki/Elo_rating_system
const K = 32;
function getEloChange(winner: number, loser: number): number {
	const eloChange = Math.round(K * (1 - ExpectationToWin(winner, loser)));
  	return eloChange !== 0 ? eloChange : 1; //Minimum 1 elo change
}

function ExpectationToWin(winner: number, loser: number): number {
  return 1 / (1 + Math.pow(10, (loser - winner) / 400.0));
}

function hasDuplicates(teams: Team[]): boolean {
    const idSet = new Set<string>();
  
    for (const team of teams) {
      if (hasDuplicatePlayers(team.players, idSet)) {
        return true;
      }
    }
  
    return false;
  }

  function hasDuplicatePlayers(players: Player[], duplicatesSet: Set<string>): boolean {
  
    for (const player of players) {
      if (duplicatesSet.has(player.id)) {
        return true;
      }
      duplicatesSet.add(player.id);
    }
  
    return false;
  }

